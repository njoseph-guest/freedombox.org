_model: page
---
title: About
---
body:

Launched in 2010 by Prof. Eben Moglen, FreedomBox is a global project to empower
regular people to reassert control over the infrastructure of the internet.
FreedomBox empowers its users to avoid the data mining, censorship and
surveillance by centralized silos that characterize the web of today. It makes
web servers personal, affordable and manageable, so that a user can host
necessary web services at home on a device they own, powered by free software
they can trust.

FreedomBox is made up of two things: a free and open source software system and
inexpensive hardware. The costs of computer processors and network bandwidth
have both fallen to such low levels that hosting your own digital services is
now affordable.

FreedomBox is a Debian Pure Blend, which means that it distributes its software
through Debian repositories. By building FreedomBox directly within the Debian
GNU/Linux ecosystem, we benefit from the contributions of the <a
href="https://contributors.debian.org/">1,000+ active Debian contributors</a>,
in addition to Debian's high standards for quality assurance and security.
FreedomBox's major contribution to the Debian ecosystem is providing a user
interface that simplifies and automates otherwise complicated administrative
tasks associated with running a server, like installing applications and
applying updates.

If you want to join the community of FreedomBox contributors or <a
href="https://freedomboxfoundation.org/donate/">support the non-profit
Foundation behind them</a> – get in touch with us on our <a
href="https://discuss.freedombox.org/">forum</a>, <a
href="https://alioth-lists.debian.net/cgi-bin/mailman/listinfo/freedombox-discuss">mailing
list</a>, <a href="https://wiki.debian.org/FreedomBox/Support">IRC channel</a>,
or <a href="https://salsa.debian.org/freedombox-team">GitLab instance</a>.
